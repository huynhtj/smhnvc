<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>SMH Neurovascular Centre</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/3/flatly/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="./css/custom.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>    
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
  <link href="./css/xeditable.min.css" rel="stylesheet">
  <script src="./js/xeditable.min.js"></script>  

    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.js"></script>
    <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.16.1/select.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.16.1/select.min.css" rel="stylesheet" media="screen">    
  </head>
  <body ng-app="myApp" ng-controller="myController" ng-cloak>

<!-- #include file=".\includes\header.asp" -->

    <div class="container">

      <div class="page-header" id="banner">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <p class="lead">Aneurysm Database</p>
            <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>Last Name</th>
                          <th>First Name</th>
                          <th>Age</th>
                          <th>Aneurysm</th>
                          <th>Date of SAH</th>
                          <th>Last visit/imaging</th>
                          <th>Staff</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr ng-repeat="x in allaneurysms">
                          <td>{{x.lastname}}</td>
                          <td>{{x.firstname}}</td>
                          <td>{{calculateAge(getDate(x.birthdate))}}</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
          </div>

        </div>

      </div>
    </div>


    <script src="./controller/aneurysm-list_controller.js" type="text/javascript"></script>
    <script src="./services/services.js" type="text/javascript"></script>    
      </body>
</html>
