    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="index.asp" class="navbar-brand">SMH Neurovascular Centre</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="aneurysm">Aneurysm <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="aneurysm">
                <li><a href="aneurysm-new.asp">New Patient/Case</a></li>
                <li><a href="#">Follow-up</a></li>
                <li class="divider"></li>
                <li><a href="#">Scheduled Cases</a></li>
                <li class="divider"></li>
                <li><a href="aneurysm-list.asp">All Patients</a></li>
                <li><a href="#">Conference Cases</a></li>
              </ul>
            </li>
            <li>
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="carotid">Carotid <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="carotid">
                <li><a href="#">New Patient/Case</a></li>
              </ul>
            </li>
            <li>
              <a href="#">Stroke</a>
            </li>
            <li>
              <a href="#">AngioQI</a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Logout</a></li>
          </ul>
        </div>
      </div>
    </div>