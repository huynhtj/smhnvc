<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.asp">SMH Neurovascular Centre</a>
    </div>
    <ul class="nav navbar-nav">
      <li ng-class="getMenuClass('categories.php')"><a href="categories.php">Aneurysm</a></li>
      <li ng-class="getMenuClass('tags.php')"><a href="tags.php">Carotid</a></li>
      <li ng-class="getMenuClass('caselist.php')"><a href="caselist.php">Stroke</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
    </ul>
  </div>
</nav>