angular.module('myApp').factory('myService', ['$http', '$q', '$rootScope', function($http, $q, $rootScope) {
	var myService = {};

	myService.getallaneurysmpt = function() {
		return $http({
      		method: 'GET',
      		url: './api/getallaneurysmpt.asp'
   			}).then(function (success){
   				return success;
		   },function (error){

		   });
	};


	myService.aneurysmlocations = function() {
		return $http({
      		method: 'GET',
      		url: './api/aneurysmlocations.asp'
   			}).then(function (success){
   				return success;
		   },function (error){

		   });
	};


	return myService;
}]);