<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>SMH Neurovascular Centre</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/3/flatly/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="./css/custom.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
    <link href="./css/xeditable.min.css" rel="stylesheet">
    <script src="./js/xeditable.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.js"></script>
    <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.16.1/select.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.16.1/select.min.css" rel="stylesheet" media="screen">
    <style>
    .full button span {
    background-color: limegreen;
    border-radius: 32px;
    color: black;
    }
    .partially button span {
    background-color: orange;
    border-radius: 32px;
    color: black;
    }
    </style>
  </head>
  <body ng-app="myApp" ng-controller="myController" ng-cloak>
    <!-- #include file=".\includes\header.asp" -->
    <div class="container">
      <div class="page-header" id="banner">
        <div class="row">
          <div class="col-lg-8 col-md-7 col-sm-6">
            <p class="lead">Aneurysm Database - New Patient</p>
          </div>
        </div>
        <div class="well col-lg-8 col-md-7 col-sm-6">
          <form class="form-horizontal">
            <fieldset>
              <legend>Demographics</legend>
              <div class="form-group">
                <label for="lastname" class="col-lg-2 control-label">MRN</label>
                <div class="col-lg-4">
                  <input type="text" class="form-control" id="mrn" placeholder="MRN" autocomplete="off">
                </div>
                
              </div>
              <div class="form-group">
                <label for="lastname" class="col-lg-2 control-label">Last Name</label>
                <div class="col-lg-4">
                  <input type="text" class="form-control" id="lastname" placeholder="Last Name" autocomplete="off">
                </div>
                <label for="firstname" class="col-lg-2 control-label">First Name</label>
                <div class="col-lg-4">
                  <input type="text" class="form-control" id="firstname" placeholder="First Name" autocomplete="off">
                </div>
                
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Birthday</label>
                <div class="col-lg-4">
                  <p class="input-group">
                    <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" />
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                    </span>
                  </p>
                </div>
                <label class="col-lg-2 control-label">Age</label>
                <div class="col-lg-4">
                  <input type="text" class="form-control" id="firstname" autocomplete="off" value="{{calculateAge(dt)}}" disabled="">
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Gender</label>
                <div class="col-lg-10">
                  <div class="radio-inline">
                    <label>
                      <input type="radio" name="gender" id="optionsRadios1" value="" checked="0">
                      Male
                    </label>
                  </div>
                  <div class="radio-inline">
                    <label>
                      <input type="radio" name="gender" id="optionsRadios2" value="1">
                      Female
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Population</label>
                <div class="col-lg-10">
                  <div class="radio">
                    <label>
                      <input type="radio" name="population" id="population1" value="" checked="0">
                      North American, European (other than Finnish)
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="population" id="population2" value="1">
                      Japanese
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="population" id="population3" value="2">
                      Finnish
                    </label>
                  </div>
                </div>
              </div>
              <legend>Medical History</legend>
              <div class="form-group">
                <label for="hypertension" class="col-lg-2 control-label">Comorbidities</label>
                <div class="checkbox-inline">
                  <label>
                    <input type="checkbox" name="hypertension"> Hypertension
                  </label>
                </div>
                <div class="checkbox-inline">
                  <label>
                    <input type="checkbox" name="priorSAH"> Prior SAH
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Smoking</label>
                <div class="col-lg-3">
                  <div class="radio-inline">
                    <label>
                      <input type="radio" name="smoking" id="optionsRadiosSmoke1" value="" checked="0">
                      No
                    </label>
                  </div>
                  <div class="radio-inline">
                    <label>
                      <input type="radio" name="smoking" id="optionsRadiosSmoke2" value="1">
                      Yes
                    </label>
                  </div>
                </div>
                <label for="packyears" class="col-lg-2 control-label">Pack Years</label>
                <div class="col-lg-2">
                  <input type="text" class="form-control" id="packyears" placeholder="0" autocomplete="off">
                </div>
              </div>
              <legend>Aneurysm Characteristics</legend>
              <div class="form-group">
                <label for="select" class="col-lg-3 control-label">Number of Aneurysms</label>
                <div class="col-lg-2">
                  <select class="form-control" id="select">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                  </select>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">Panel heading</div>
                <div class="panel-body">
                  Panel content
                </div>
              </div>
              <div class="form-group">
                <label class="col-lg-2 control-label">Location</label>
                <div class="col-lg-10">
                  <div class="radio" ng-repeat="y in aneurysmlocations">
                    <label>
                      <input type="radio" name="population" id="population1" value="{{y.ID}}" checked="0">
                      {{y.Location}}
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="textArea" class="col-lg-3 control-label">Size</label>
                <div class="col-lg-3">
                  <input type="text" class="form-control" id="lastname" placeholder="Size 1 (largest)" autocomplete="off">
                </div>
                <div class="col-lg-2">
                  <input type="text" class="form-control" id="lastname" placeholder="Size 2" autocomplete="off">
                </div>
                <div class="col-lg-2">
                  <input type="text" class="form-control" id="lastname" placeholder="Size 3" autocomplete="off">
                </div>
              </div>
              <div class="form-group">
                <label for="textArea" class="col-lg-3 control-label">Aneurysm Neck</label>
                <div class="col-lg-2">
                  <input type="text" class="form-control" id="lastname" placeholder="Last Name" autocomplete="off">
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-body">
                  PHASES Score:
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-10 col-lg-offset-5">
                  <button type="reset" class="btn btn-default">Cancel</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
      <script src="./controller/aneurysm-new_controller.js" type="text/javascript"></script>
      <script src="./services/services.js" type="text/javascript"></script>
    </body>
  </html>