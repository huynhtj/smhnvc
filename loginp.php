<?php
//include_once( 'checkiflogin.php'); ?>


	<!DOCTYPE html>
	<html lang="en">

	<head>
		<title>UW Neuroradiology Cases</title>
		<link rel="stylesheet" href="https://bootswatch.com/3/slate/bootstrap.min.css" />
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" />

		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
		<script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<link href="./css/xeditable.min.css" rel="stylesheet">
		<script src="./js/xeditable.min.js"></script>
	</head>

	<body ng-app="myApp" ng-controller="myController" ng-cloak>


			
		<div class="col-md-4 col-md-offset-4">
					<div class="page-header">
			<h1>
				UW Neuroradiology Cases
			</h1>
			</div>
			<div class="form-group well bs-component">
				<h2>
					Login
				</h2>
				<h4>
					<?php if (!empty($login_error)) echo $login_error;?>
				</h4>
				<form class="form-horizontal" action="login.php" method="post">
					<fieldset>
						<div class="form-group">
							<label for="inputEmail" class="col-lg-3 control-label">Username</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="inputEmail" name="username" placeholder="Username">
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword" class="col-lg-3 control-label">Password</label>
							<div class="col-sm-9">
								<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-9 col-md-offset-3">
								<button type="submit" class="btn btn-primary">Login</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>






	</body>
	<script>
		var app = angular.module('myApp', ["xeditable"]);
		app.controller('myController', function($scope, $http, $filter) {


		});
	</script>

	</html>