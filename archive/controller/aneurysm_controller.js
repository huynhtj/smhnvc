$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});

	var app = angular.module('myApp', ["xeditable"]);

	app.controller('myController', ['$scope', '$http', '$filter', 'myService', function($scope, $http, $filter, myService) {

		$scope.refreshcases = function() {
			myService.getcarotids().success(function(resp) {
				$scope.cases = resp.data;
				console.log($scope.cases);
			});
		}

		$scope.refreshcases();

	}]);