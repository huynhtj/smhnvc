<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" ng-app="myApp">

<head>

	<?php require './includes/title.php' ?>
	<link rel="stylesheet" href="https://bootswatch.com/3/slate/bootstrap.css" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="./js/modernizr.js"></script>
	<link rel="stylesheet" type="text/css" href="./css/casev2.css" />
	<script>
		// set options for html5shiv
		if (!window.html5) {
			window.html5 = {};
		}
		window.html5.shivMethods = false;
	</script>

	    <!-- Icons -->

    <style>
      @font-face {
        font-family: 'Glyphicons Halflings';
        src:url('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/fonts/glyphicons-halflings-regular.eot');
        src:url('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'),
          url('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/fonts/glyphicons-halflings-regular.woff') format('woff'),
          url('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/fonts/glyphicons-halflings-regular.ttf') format('truetype'),
          url('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/fonts/glyphicons-halflings-regular.svg#glyphicons-halflingsregular') format('svg');
      }
    </style>
	
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-tags-input/3.1.1/ng-tags-input.min.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/ng-tags-input/3.1.1/ng-tags-input.min.css" rel="stylesheet" media="screen">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/ng-tags-input/3.1.1/ng-tags-input.bootstrap.min.css" rel="stylesheet" media="screen">
	
	
	<script src="https://code.angularjs.org/1.2.28/angular-resource.min.js"></script>
	<link href="./css/xeditable.min.css" rel="stylesheet">
	<script src="./js/xeditable.min.js"></script>
	<script type="text/javascript">
		var scroll = 0;
	</script>
	<script type="text/javascript" src="./js/default.js"></script>

</head>


<body ng-controller="myController" ng-cloak>
	<!--#include file="./includes/header.asp" -->
	<div class="container">

	</div>





		<script src="./controller/index_controller.js" type="text/javascript"></script>

</html>