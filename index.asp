<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>SMH Neurovascular Centre</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://bootswatch.com/3/flatly/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="./css/custom.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
    <link href="./css/xeditable.min.css" rel="stylesheet">
    <script src="./js/xeditable.min.js"></script>
  </head>
  <body ng-app="myApp" ng-controller="myController" ng-cloak>
    <!-- #include file=".\includes\header.asp" -->
    <div class="container">
      <div class="page-header" id="banner">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <h2>Dashboard</h2>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Current Intervention/Stroke Cases <small><span class="label label-warning">Live Case</span></small></h3>
              </div>
              <div class="panel-body">
                <table class="table table-striped table-hover ">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Column heading</th>
                      <th>Column heading</th>
                      <th>Column heading</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <h3>Upcoming Schedule</h3>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Interventions</h3>
              </div>
              <div class="panel-body">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#intpend" data-toggle="tab" class="text-danger">Pending Sched. <span class="badge">40</span></a></li>
                  <li><a href="#intsched" data-toggle="tab">Scheduled <span class="badge">10</span></a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade active in" id="intpend">
                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="intsched">
                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Imaging</h3>
                <!-- example Park --> <!-- Pacheco -->
              </div>
              <div class="panel-body">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#intpend" data-toggle="tab" class="text-danger">Pending Sched. <span class="badge">40</span></a></li>
                  <li><a href="#intsched" data-toggle="tab">Scheduled <span class="badge">10</span></a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade active in" id="intpend">
                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="intsched">
                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Clinic Visits <span class="badge">42</span></h3>
                <!-- Macdonald/Mapoy -->
              </div>
              <div class="panel-body">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#intpend" data-toggle="tab" class="text-danger">Pending Sched. <span class="badge">40</span></a></li>
                  <li><a href="#intsched" data-toggle="tab">Scheduled <span class="badge">10</span></a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade active in" id="intpend">
                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="intsched">
                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title">Conference Cases <span class="badge">42</span></h3>
                <!-- Macdonald/Mapoy -->
              </div>
              <div class="panel-body">
                <table class="table table-striped table-hover ">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Column heading</th>
                      <th>Column heading</th>
                      <th>Column heading</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="./controller/index_controller.js" type="text/javascript"></script>
    <script src="./services/services.js" type="text/javascript"></script>
  </body>
</html>